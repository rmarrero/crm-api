<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Upload is the model behind the upload image.
 *
 * @property User|null $user This property is read-only.
 *
 */
class UploadImage extends Model
{
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'],'required'],
            [
                ['imageFile'],
                'file',
                'skipOnEmpty' => 'false',
                'mimeTypes' => 'image/png,image/jpg',
            ],
        ];
    }
}