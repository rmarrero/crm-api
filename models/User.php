<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property bool $is_admin
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const PASSWORDSALT = 'eqetnnnhrw3LH';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_admin'],'boolean'],
            [['username', 'password'], 'required'],
            [['username', 'password'], 'string', 'max' => 100],
        ];
    }

    public function beforeSave($insert)
    {
        if(isset($this->dirtyAttributes['password'])){
            $passHashed = $this->passwordHashed($this->password);
            $this->password = $passHashed;
        }
        return parent::beforeSave($insert);
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'is_admin' => 'Admin',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $this->passwordHashed($password);
    }

    /**
     * Create a Hash from string. It is used to save passwords as no text plain
     * @param string $passpword text plain.
     * @return string Hash password
     */
    public static function passwordHashed($password)
    {
        return md5(self::PASSWORDSALT.$password);
    }

    /**
     * Check if user is admin
     *
     * @return bool if user is admin returns true
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }
}
