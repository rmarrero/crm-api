<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $photo
 * @property int $created_by
 * @property int $modified_by
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'],'unique'],
            [['id'],'match','pattern' => '/^[a-z0-9]+$/i','message' => 'ID field only permit letters and numbers'],
            [['id','name', 'surname'], 'required'],
            [['created_by', 'modified_by'], 'integer'],
            [['id','name', 'surname'], 'string', 'max' => 100],
            [['photo'], 'string', 'max' => 200],
            [['photo'], 'url', 'defaultScheme' => 'http', 'when' => function($model){
                return !(YII_DEBUG && (0 === strpos($model->photo,'http://localhost')));
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'photo' => 'Photo',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }

    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert)){
            return false;
        }
        
        if(!Yii::$app->user->isGuest){
            $field = $insert ? 'created_by' : 'modified_by';
            $this->$field = Yii::$app->user->id;
            return true;
        } else {
            $this->addError('Authentication','I do not know who are you');
        }

        return false;
    }

    public function afterDelete()
    {
        if($this->photo){
            Image::deleteByUrl($this->photo);
        }
        parent::afterDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(!$insert &&  isset($changedAttributes['photo'])){
            Image::deleteByUrl($changedAttributes['photo']);
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
