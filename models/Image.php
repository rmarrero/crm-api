<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string $name
 * @property string $full_name
 * @property string $url
 * @property string $create_at
 */
class Image extends \yii\db\ActiveRecord
{
    const RELATIVEPATHIMAGE = 'assets/uploads/';
    const ROOTFILE = '@app/web/';
    const ROOTWEB = '@web/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['create_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['url','full_name'], 'string', 'max' => 200],
            [['url'], 'url', 'defaultScheme' => 'http', 'when' => function($model){
                return !(YII_DEBUG && (0 === strpos($model->url,'http://localhost')));
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'full_name' => 'Full Name',
            'url' => 'Url',
            'create_at' => 'Create At',
        ];
    }

    public function afterDelete()
    {
        $this->removeImage();
        parent::afterDelete();
    }

    public function removeImage()
    {
        $file = Yii::getAlias(self::ROOTFILE) . $this->full_name;
        if(is_file($file)){
            unlink($file);
        }
    }

    public static function deleteByUrl($url)
    {
        $file = static::findOne(['url' => $url]);
        if($file){
            $file->delete();
        }
    }
    /**
     * Create a new register from an upload Image
     * @param app\models\UploadImage $uploadImage the model used to handle de upload image
     * @return string|false the full name of the file or false if error.
     */
    public function saveUploadImage(UploadImage $uploadImage)
    {
        $date = new \DateTime();
        $name = $this->generateFileName($date);
        $path = $this->generatePath($date);
        $fullName = $path . $name . '.' . $uploadImage->imageFile->extension;

        if (!$uploadImage->imageFile->saveAs(Yii::getAlias(self::ROOTFILE) . $fullName)){
            $this->addError('imageFile','cannot save image.');
            return false;
        }

        $this->name = $name;
        $this->full_name = $fullName;
        $this->url = \yii\helpers\Url::to(Yii::getAlias(self::ROOTWEB) . $fullName, true);

        return $this->save();

    }

    private function generateFileName(\DateTime $date)
    {
        return Yii::$app->security->generateRandomString(10) . $date->format('his');
    }
    
    private function generatePath(\DateTime $date, $relative = self::RELATIVEPATHIMAGE)
    {
        $path = $relative . $date->format('Y/m/d/');
        if(!is_dir(Yii::getAlias(self::ROOTFILE) . $path)){
            mkdir(Yii::getAlias(self::ROOTFILE) . $path,0777,true);
        }
        return $path;
    }

    
}
