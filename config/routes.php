<?php

return [
    [
        'class' => 'yii\rest\UrlRule', 
        'controller' => 'v1/customer',
        'tokens' => ['{id}' =>'<id:\\w+>'],
    ],
    ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/user'],
    ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/image'],
];