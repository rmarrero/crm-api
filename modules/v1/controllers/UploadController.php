<?php

namespace app\modules\v1\controllers;

use Yii;
use app\models\UploadImage;
use yii\web\UploadedFile;
use app\models\Image;
use yii\filters\auth\HttpBasicAuth;
use app\models\User;

class UploadController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if($action->id == 'image'){
            $this->enableCsrfValidation = false;
        } 
        
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username,$password){
                $user = User::findByUsername($username);
                if (null !== $user && $user->validatePassword($password)){
                    return $user;
                }
                return null;
            }
        ];
        return $behaviors;
    }

    public function actionImage()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if(!Yii::$app->request->isPost){
            throw new \yii\web\NotFoundHttpException();
        }

        $uploadedImage = new UploadImage();
        $uploadedImage->imageFile = UploadedFile::getInstanceByName('imageFile');

        if($uploadedImage->validate()){
            $image = new Image();
            if($image->saveUploadImage($uploadedImage)){
                return [
                    "location" => $image->url,
                ]; 

            }elseif($image->hasErrors()){
                return $image->errors;
            }
        } elseif ($uploadedImage->hasErrors()){
            return $uploadedImage->errors;
        }
        
        throw new \yii\web\HttpException(503, 'the server is currently unable to handle the request');

    }

}