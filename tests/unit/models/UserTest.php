<?php

namespace tests\unit\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(1));
        expect($user->username)->equals('admin');

        expect_not(User::findIdentity(999));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('admin'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('admin');

        expect_that($user->validatePassword('admin'));
        expect_not($user->validatePassword('123456'));        
    }

    public function testPasswordHashed()
    {
        $pass = 'hola';
        $passHashed = User::passwordHashed($pass);
        $this->assertEquals('aa7d40a7fc9b5046c32fa08a01e49626',$passHashed);
    }


}
