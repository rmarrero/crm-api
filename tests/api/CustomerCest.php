<?php


class CustomerCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function createCustomer(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/customers',['name'=>'Nombre','surname'=>'Apellido','id'=>'test1']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"name":"Nombre","surname":"Apellido","id":"test1","created_by":1}');
    }

    public function createCustomerWithPhoto(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/customers',[
            'name'=>'Nombre',
            'surname'=>'Apellido',
            'id'=>'test1',
            'photo' => 'https://www.html5rocks.com/static/images/tutorials/easy-hidpi/chrome1x.png'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"name":"Nombre","surname":"Apellido","id":"test1","photo":"https://www.html5rocks.com/static/images/tutorials/easy-hidpi/chrome1x.png","created_by":1}');
    }

    public function listCustomers(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/customers',['name'=>'Nombre','surname'=>'Apellido','id'=>'test1']);
        $I->amHttpAuthenticated('admin','admin');
        $I->sendGET('/customers');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('[{"id":"test1","name":"Nombre","surname":"Apellido","photo":null,"created_by":1,"modified_by":null}]');
    }

    public function viewCustomer(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/customers',['name'=>'Nombre','surname'=>'Apellido','id'=>'test1']);
        $I->amHttpAuthenticated('admin','admin');
        $I->sendGET('/customers/test1');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":"test1","name":"Nombre","surname":"Apellido","photo":null,"created_by":1,"modified_by":null}');
    }

    public function modifyCustomer(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/customers',['name'=>'Nombre','surname'=>'Apellido','id'=>'test1']);
        $I->deleteHeader('Authorization');
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPUT('/customers/test1',['name'=>'Nombre2']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":"test1","name":"Nombre2","surname":"Apellido","photo":null,"created_by":1,"modified_by":1}');
    }

    public function deleteCustomer(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/customers',['name'=>'Nombre','surname'=>'Apellido','id'=>'test1']);
        $I->deleteHeader('Authorization');
        $I->amHttpAuthenticated('admin','admin');
        $I->sendDELETE('/customers/test1');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NO_CONTENT); // 204
    }

    public function noAuthenticatedRequest(ApiTester $I)
    {
        $I->sendGET('/customers');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
    }


}
