<?php


class UserCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function createUser(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPOST('/users',['username'=>'usuario1','password'=>'pass']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['username'=>'usuario1','password'=>'441f76a83b7d18be17003c0661d0d661']);
    }
    
    public function listUsers(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendGET('/users');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('[{"id":1,"username":"admin","password":"458f557766b8e88bbc79dd1b1b9d5c07","is_admin":1},{"id":2,"username":"prueba","password":"21a55c01741d8a20bc2a1aec061320af","is_admin":0}]');
    }

    public function viewUser(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendGET('/users/1');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":1,"username":"admin","password":"458f557766b8e88bbc79dd1b1b9d5c07","is_admin":1}');
    }

    public function modifyUser(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPUT('/users/2',['is_admin'=>'1']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":2,"username":"prueba","password":"21a55c01741d8a20bc2a1aec061320af","is_admin":"1"}');
    }

    public function modifyUserPassword(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendPUT('/users/2',['password'=>'hola']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":2,"username":"prueba","password":"aa7d40a7fc9b5046c32fa08a01e49626","is_admin":0}');
    }

    public function deleteUser(ApiTester $I)
    {
        $I->amHttpAuthenticated('admin','admin');
        $I->sendDELETE('/users/2');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NO_CONTENT); // 204
    }

    public function noAuthenticatedRequest(ApiTester $I)
    {
        $I->sendGET('/users');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
    }


    public function requestFromNoAdminUser(ApiTester $I)
    {
        $I->amHttpAuthenticated('prueba','prueba');
        $I->sendGET('/users');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
    }


}
