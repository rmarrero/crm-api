# CRM-API

This is a REST API to manage customer data for a small shop. With this api one Authorized user will be able to create, list, modify and delete customers.

If the authenticated user is also an admin he will be able to manage the rest of the users, that means he will able to create, delete, update and list users and even to change admin his status.

Also the API allows to upload images to use for customers.

## INSTALLATION

### Install via composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install the dependencies using the following command:

~~~
php composer.phar install
~~~

We recommend to be installed a LAMP stack with apache mod_rewrite enabled.




## CONFIGURATION

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=crmapi',
    'username' => 'crmapi',
    'password' => '1234',
    'charset' => 'utf8',
];
```
You have a database copy in the file `database/crmapi.sql` which includes two users.
|user|pass|
|-|-|
|admin|admin
|prueba|prueba


## HOW TO USE

### Authentication

The API always uses Basic Auth Authentication so if you use the `admin` user (password `admin`) you will need to add to the header of request

```
Authorization: Basic YWRtaW46YWRtaW4=
```

### RESTful URLs

---
* Create customer
    * POST <http://api.example.com/v1/customer>
* List customers
    * GET <http://api.example.com/v1/customer>
* Wiew customer
    * GET <http://api.example.com/v1/customer/:id>
* Update customer
    * PUT <http://api.example.com/v1/customer/:id>
* Delete customer
    * DELETE <http://api.example.com/v1/customer/:id>

* custumer fields

    |field|type|required
    |-|-|-|
    |id|string (ony letters and numbers)|yes
    |name|string|yes
    |surname|string|yes
    |photo|url|no
---

* Create user
    * POST <http://api.example.com/v1/users>
* List users
    * GET <http://api.example.com/v1/users>
* Wiew user
    * GET <http://api.example.com/v1/users/:id>
* Update user
    * PUT <http://api.example.com/v1/users:id>
* Delete user
    * DELETE <http://api.example.com/v1/users:id>

* User fields
    |field| type|required
    |-|-|-|
    |username|string|yes
    |password|string|yes
    |admin|boolean|no (default no)
---

* Upload image
    * POST <http://api.example.com/v1/upload/image>

        This request is by *form-data* and the name of de key is `imageFile` and its value the image to upload.
        
        The response is a json like this:
        
        ```
        {"location":"http://localhost/crm-api/web/assets/uploads/2019/06/27/m0Rxth_s94092223.png"}
        ```
* List images
    * GET <http://api.example.com/v1/images>
* Delete image
    * DELETE <http://api.example.com/v1/images/:id>

---

### Examples

Imagine that you want to create a new customer with local photo

Request:
```
curl -X POST \
  http://api.example.com/v1/upload/image \
  -H 'Authorization: Basic cHJ1ZWJhOnBydWViYQ==' \
  -H 'content-type: multipart/form-data;' \
  -F 'imageFile=@/path/to/image.jpg'
```
Respose:
```
{"location":"http://api.example.com/assets/uploads/2019/06/26/ApsE657TnR110430.png"}
```
Request:
```
curl -X POST \
  http://api.example.com/v1/customers \
  -H 'Content-Type: application/json' \
  -d '{"id":"test","surname":"Perez","name":"Paco", "photo": "http://api.example.com/assets/uploads/2019/06/26/ApsE657TnR110430.png"}'
  ```
  Response:
  ```
  {"id":"test","surname":"Perez","name":"Paco","photo": "http://api.example.com/assets/uploads/2019/06/26/ApsE657TnR110430.png","created_by":1}
  ```
  Or if you want to change admin status to the user with ID 2

  Request:
  ```
  curl -X PUT \
  http://api.example.com/v1/users/2 \
  -H 'Authorization: Basic YWRtaW46YWRtaW4=' \
  -H 'Content-Type: application/json' \
  -d '{"is_admin":true}'
  ```
  Response:
  ```
  {"id":2,"username":"prueba","password":"adfn34tagSrgsse","is_admin":true}
  ```


## TESTING

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).

You can execute all tests with the following command:

```
./vendor/bin/codecept run
```

**NOTE**

You must configurate a new database for testing. Please edit the file `config/test_db` and populate the new database with `database/crmapi`